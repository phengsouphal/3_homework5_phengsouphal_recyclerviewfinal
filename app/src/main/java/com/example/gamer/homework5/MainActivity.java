package com.example.gamer.homework5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.gamer.homework5.Adapter.AdapterUniversity;
import com.example.gamer.homework5.AddUniversityData.AddUniverAdapter;
import com.example.gamer.homework5.CalledBack.CallBackUniversity;
import com.example.gamer.homework5.Entity.DataSchool;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CallBackUniversity,CallBackUniversity.AddDialogUniversity {

    List<DataSchool> dataSchoolList;
    RecyclerView recyclerView;
    AdapterUniversity adapterUniversity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.rvUniversity);

        dataSchoolList=new ArrayList<>();
        adapterUniversity=new AdapterUniversity(dataSchoolList,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(adapterUniversity);

        recyclerView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getApplicationContext(),"Long Clivk",Toast.LENGTH_SHORT).show();
                return false;
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.topmenu:{
                AddUniverAdapter addUniverAdapter=new AddUniverAdapter();
                addUniverAdapter.show(getSupportFragmentManager(),"Add New University");
            }
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void delete(DataSchool dataSchool, int postion) {
        dataSchoolList.remove(dataSchool);
        adapterUniversity.notifyItemRemoved(postion);
    }

    @Override
    public void getUniversity(DataSchool dataSchool) {
        dataSchoolList.add(0,dataSchool);
        adapterUniversity.notifyDataSetChanged();
    }
}
